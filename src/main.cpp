
#include "main.h"
#include "rs232_term.h"
#include "service.h"
#include "devices.h"
#include "config.h"
#include "wiringPi.h"
#include "senson.h"


#define DEBUG_SOME_CODE

#define SDCARD_LOG
#define SEARCH_CORRECT_TTYUSB
#define SEND_ALL_TO_REMOTE
//#define DISABLE_REMOTE



int main(int argc, char ** argv)
{
    FILE *fp;
    struct termios       tioSenson_b[SENS_MAX_QTY] {},
                         tioRemote_b[SERIAL_PORTS_MAX_QTY] {};

    int         fdRemotes[SERIAL_PORTS_MAX_QTY] = { 0, };

    char        strSens[READ_BUFFER_SIZE]       = { 0, },   outputStr[OUT_STR_LENGTH]       = { 0, },
                timeSting[64]                   = { 0, },   configPath[READ_BUFFER_SIZE] = { 0, };

    char    serialPortsList[SERIAL_PORTS_MAX_QTY][SERIAL_PORT_MAX_NAME_LEN] = { 0, };
    ssize_t serialPortsListQty = 0;


    struct  Senson sensorList[SENS_MAX_QTY] = { 0, };
    ssize_t sensorsQty = 0;

    bool panicMode = false;
    time_t      timeStart, timeEnd;
    double seconds_dbl = 0;


    // Setting up GPIOs
    wiringPiSetup();
    pinMode(LED_SENSOR, OUTPUT);
    digitalWrite(LED_SENSOR, LOW);
    pinMode(LED_GPS, OUTPUT);
    digitalWrite(LED_GPS, LOW);
    pinMode(LED_EXT_TX, OUTPUT);
    digitalWrite(LED_EXT_TX, LOW);
    pinMode(BTN_POWER, INPUT);
    pullUpDnControl(BTN_POWER, PUD_DOWN);

    /***********************************************************
    *********     Reading config and creating files    *********
    ************************************************************/

    if (argc == 2) {
        memset(configPath, 0, 64);
        memcpy(configPath, argv[1], minOfTwo(strlen(argv[1]), READ_BUFFER_SIZE));
        strcat(configPath, "config");

        fp = fopen(argv[1], "r");
        if (fp != NULL) {
            fclose(fp);
        }
        else {
            fp = fopen(configPath, "r");
            if (fp != NULL) {
                memset(configPath, 0, strlen(configPath));
                strcat(configPath, "Couldn't open config file! Using default parameters!\n");
            }
        }
    }

    printf("Config file path: %s\n", configPath);


    // Looking for serial ports available in system
    serialPortsListQty = sPortListPorts("/dev/ttyUSB", serialPortsList, serialPortsListQty);
    serialPortsListQty = sPortListPorts("/dev/ttyAMA", serialPortsList, serialPortsListQty);

    sprintf(outputStr, "\nAvailable ports:");
    for (ssize_t i = 0; i < serialPortsListQty; ++i) {
        if (OUT_STR_LENGTH - strlen(outputStr) - 5 > strlen(serialPortsList[i])) {
            strcat(outputStr, "\n\t");
            strcat(outputStr, serialPortsList[i]);
        }
    }
    printf("%s\n", outputStr);


    /***********************************************************
    ********     Looking for and setting up sensors    *********
    ************************************************************/
    printf("Sensor...\n");

    for (ssize_t sens_i = 0; sens_i < SENS_MAX_QTY; ++sens_i) {
        for (ssize_t port = 0; port < serialPortsListQty; ++port) {
            if (NULL != strstr(serialPortsList[port], "PortIsUsed"))
                continue;
            if (sensVerifyPort(serialPortsList[port],
                         &tioSenson_b[sens_i],
                         &sensorList[sens_i])) {
                memset(serialPortsList[port], 0, SERIAL_PORT_MAX_NAME_LEN);
                strcpy(serialPortsList[port], "PortIsUsed");
                sensorsQty++;
                break;
            }
            else {
                sensDeInit(&sensorList[sens_i], &tioSenson_b[sens_i]);
            }
        }
    }
    if (sensorsQty > 0) {
        sensInitMult(sensorList);
        sensSetupFriendlyNames(sensorList, configPath);
        sensSetupLimits(sensorList, configPath);
        for (ssize_t sens_i = 0; sens_i < SENS_MAX_QTY; ++sens_i) {
            if (sensorList[sens_i].fd > 0) {
                sensorList[sens_i].blinkLed = true;
                sprintf(outputStr, "%s\n\n", sensPrintInfo(&sensorList[sens_i]));
                printf(outputStr);
            }
        }
    }
    else {
        sprintf(outputStr, "Error. Sensor was not found at all possible ports. Exit.\n");
        printf(outputStr);
        return -52;
    }

    /***********************************************************
    ********************     Main cycle     ********************
    ************************************************************/

    while (1) {

        getDateTimeWithMs_str(timeSting, 64, (char *)("%Y %m %d %H:%M:%S"), 1);

        sprintf(outputStr, "%s", sensGetConcMult(sensorList, &panicMode));

        printf("%s", outputStr);


// Clearing buffers

        memset(strSens, 0, READ_BUFFER_SIZE);
        memset(outputStr, 0, READ_BUFFER_SIZE);

        ledToggle(LED_SENSOR);
        usleep(300 * 1000);

        if (LOW == digitalRead(BTN_POWER)) {
            time(&timeStart);
            time(&timeEnd);
        }
        else {
            time(&timeEnd);
        }

        seconds_dbl = difftime(timeEnd, timeStart);
        if (seconds_dbl > 10)
            break;
    }

    sensDeinitMult(sensorList, tioSenson_b);

    for (ssize_t i = 0; i < SERIAL_PORTS_MAX_QTY; ++i) {
        if (fdRemotes[i] > 0) {
            sPortClose(fdRemotes[i], &tioRemote_b[i]);
        }
    }

    printf("Power button was pressed for 10 seconds. Quiting and powering down.\n");
    digitalWrite(LED_EXT_TX, HIGH);
    digitalWrite(LED_GPS, HIGH);
    digitalWrite(LED_SENSOR, HIGH);
    //usleep(2 * 1000 * 1000);
    for (unsigned short i = 0; i < 27; ++i) {
        ledToggle(LED_EXT_TX);
        ledToggle(LED_GPS);
        ledToggle(LED_SENSOR);
        usleep(150 * 1000);
    }
    system("shutdown -P now");
    return 0;
}

