/*
* config.cpp
*
*  Created on: Mar 11, 2018
*      Author: Andrey Loshchilov
*/
#include "main.h"
#include "config.h"

ssize_t configReadParameterStr(const char * pathToConfig,    const char * parameterName,
                                char * parameterValue,  const char * defaultValue)
{
    FILE    *fp;
    char    buffer[CONFIG_LINE_BUF_SIZE] = { 0, };
            //defaulValueBuffer[CONFIG_LINE_BUF_SIZE] = { 0, };

    char    *value, *miscPtr;

    if ((fp = fopen(pathToConfig, "r")) == NULL) {
        //printf("Failed to open config: %s\n", pathToConfig);
        memcpy(parameterValue, defaultValue, (strlen(defaultValue)));
        return -1;
    }

    while (!feof(fp)) {
        memset(buffer, 0, CONFIG_LINE_BUF_SIZE);
        fgets(buffer, CONFIG_LINE_BUF_SIZE, fp);
        miscPtr = strstr(buffer, "#");                                  // excluding comments
        if (miscPtr != NULL)
            *miscPtr = '\0';

        if (removeSpaces(buffer) == 0)                                  // Empty string
            continue;
        else if ((strstr(buffer, parameterName) == buffer) &&           
                 ((value = checkParameterString(buffer)) != NULL)) {

            memset(parameterValue, 0, CONFIG_LINE_BUF_SIZE);
            memcpy(parameterValue, value, (strlen(value)));
            return fclose(fp);
        }

    }

    memcpy(parameterValue, defaultValue, (strlen(defaultValue)));
    fclose(fp);
    return -2;
}

ssize_t configReadParameterLong(char * pathToConfig,    char * parameterName,
                                 long * parameterValue,  long defaulValue)
{
    char    buffer[CONFIG_LINE_BUF_SIZE] = { 0, };
    ssize_t err = 0;

    err = configReadParameterStr(pathToConfig, parameterName, buffer, (char *)("NaN"));

    if (err == 0) {       
        *parameterValue = strtol(buffer, NULL, 10);
        if ((*parameterValue == 0) && ((strlen(buffer) > 1) || (*buffer == '0'))) {
            *parameterValue = defaulValue;
            return -2;
        }

        return 0; 
    }
    
    *parameterValue = defaulValue;
    return err;
}

ssize_t removeSpaces(char * target)                         // Removes spaces, tabs and EOSs.
{                                                           // Returns resulting string length.
    ssize_t i = 0, j = 0, size = 0;                         // Format in quotes will be saved.
    char    buf[CONFIG_LINE_BUF_SIZE] = { 0, };             // Quotes are not saved.
    char    *ptr = target;


    size = strlen(target);
    while ((*(ptr) != '\0') && (j < size)) {
        if (*(ptr) == '\"') {
            ptr++;
            while (*(ptr) != '\"') {
                buf[i++] = *(ptr);
                j++;
                ptr++;
                if (j == size) {
                    buf[1] = '\0';          // To spoil string. if quotes were not closed.
                    break;
                }
            }
        }
        if ((*(ptr) != ' ')    &&
            (*(ptr) != '\"')   &&
            (*(ptr) != '\t')   &&
            (*(ptr) != '\r')   &&
            (*(ptr) != '\n'))
            buf[i++] = *(ptr);
        j++;
        ptr++;
    }
    
    memset(target, 0, strlen(target));
    strcat(target, buf);

    return strlen(target);
}


char * checkParameterString(char * target)  // If string contains '=', replaces it
{                                           // with '\0' and returns pointer to 
    char *ptr;                              // next symbol (i.e. parameter value).
                                            // Otherwise returns NULL.
    ptr = strstr(target, "=");
    if (ptr == NULL)
        return ptr;
    
    *ptr++ = '\0';
    
    return ptr;
}

