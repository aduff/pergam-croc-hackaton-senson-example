/*
* devices.cpp
*
*  Created on: Mar 9, 2018
*      Author: Andrey Loshchilov
*/

#include "main.h"
#include "devices.h"
#include "rs232_term.h"
#include <wiringPi.h>



ssize_t bytesAtSerialPort(int fd, ssize_t msTimeout) {
    ssize_t bytesAvailable = 0;
    ssize_t timeDiscrete = 100;  // in ms

    if (0 == msTimeout) {
        ioctl(fd, FIONREAD, &bytesAvailable);
    }
    else {
        if (msTimeout < timeDiscrete)    // minimal non-zero timeout is timeDiscrete ms
            msTimeout = timeDiscrete;

        tcflush(fd, TCIFLUSH);
        for (ssize_t i = 0; i < (msTimeout / timeDiscrete); ++i) {
            usleep(timeDiscrete * 1000);
            ioctl(fd, FIONREAD, &bytesAvailable);
            if (bytesAvailable > 0)
                break;
        }
    }
    return bytesAvailable;
}

ssize_t waitForMessageAtPort(int fd, ssize_t minimalLength, ssize_t msTimeout)
{
    ssize_t bytesAvailable = 0;
    ssize_t timeDiscrete = 20;  // in ms

    if (msTimeout < timeDiscrete)    // minimal non-zero timeout is timeDiscrete ms
        msTimeout = timeDiscrete;

    for (ssize_t i = 0; i < (msTimeout / timeDiscrete); ++i) {
        usleep(timeDiscrete * 1000);
        ioctl(fd, FIONREAD, &bytesAvailable);
        if (bytesAvailable >= minimalLength)
            break;
        else
            bytesAvailable = 0;
    }

    return bytesAvailable;
}


/*===============================================================
  =============================================================== */

speed_t selectSpeed(long speedLong, speed_t defaultSpeed)
{
    speed_t speed;

    switch (speedLong)
    {
    case 4800:
        speed = B4800;
        break;

    case 9600:
        speed = B9600;
        break;

    case 19200:
        speed = B19200;
        break;

    case 38400:
        speed = B38400;
        break;

    case 57600:
        speed = B57600;
        break;
    
    case 115200:
        speed = B115200;
        break;

    case 230400:
        speed = B230400;
        break;

    case 460800:
        speed = B460800;
        break;

    default:
        speed = defaultSpeed;
        break;
    }

    return speed;
}

/*===============================================================
  =============================================================== */

ssize_t removeEol(char * target)             
{                                            
    ssize_t size = 0;                        
    char    buf[READ_BUFFER_SIZE] = { 0, };  
    char    *ptr = target;


    size = strlen(target);

    for (ssize_t i = 0; i < size; ++i) {
        if ((*(ptr) != '\n') && (*(ptr) != '\r'))
            buf[i] = *(ptr);
        ptr++;
    }

    memset(target, 0, strlen(target));
    strcat(target, buf);

    return strlen(target);
}

/*===============================================================
  =============================================================== */

void ledToggle(int pin)
{
    if (LOW == digitalRead(pin)) {
        digitalWrite(pin, HIGH);
    }
    else {
        digitalWrite(pin, LOW);
    }
}

/*===============================================================
 *                    LMM Sensor support
  =============================================================== */
ssize_t getConcentrationLmmSensor(int fd, long sensorEnum)
{
    ssize_t bytesToRead = 0;
    char buffer[8192] = { 0, };
    long concentration = 0;
    char cmd[15] = {0x02, 0x45, 0x54, 0x43, 0x3A, // <2>ETC:FWD ?;<3><26>
                    0x46, 0x57, 0x44, 0x20, 0x3F,
                    0x3B, 0x03, 0x1A, 0x00, 0x00};
    tcflush(fd, TCIFLUSH);
    for(unsigned char i = 0; i < 5; ++i) {

        write(fd, (char *) 0x06, 1);
        usleep(3 * 1000); // 13 ms delay
        sPortWrite(fd, cmd, strlen(cmd), 5);
        //usleep(50 * 1000);
        bytesToRead = waitForMessageAtPort(fd, 200, 300);

        if (bytesToRead > 1024)
            bytesToRead = 1000;
        if (bytesToRead) {
            read(fd, buffer + strlen(buffer), bytesToRead);
            //tcflush(fd, TCIFLUSH);
            concentration = parseLmmStringForConcentration(buffer, sensorEnum);
            if (concentration < 0) {
                //printf("-1|");
                continue;
            }
            break;
            //printf("%05ld | %s\n", concentration, buffer);
        }
        else {
            //printf("-2|");
            if (4 == i)
                return -2;
        }
    }

    return concentration;
}

/*===============================================================
  =============================================================== */

long parseLmmStringForConcentration(char * string, long sensorEnum)
{
    char *ptr;
    char localBuf[8192] = { 0, };
    ssize_t bufferLength = 0;
    unsigned char requiredValueNum = 0;

    if (SENSOR_LMM == sensorEnum){
        requiredValueNum = 5;
        //requiredValueNum = 6;
    }
    else if (SENSOR_FALCON == sensorEnum) {
        requiredValueNum = 2;
    }

    bufferLength = strlen(string);
    if (bufferLength > 8191) {
        bufferLength = 8192;
    }
    memcpy(localBuf, string, bufferLength);

    ptr = strstr(localBuf, "ETC:FWD ");
    if (NULL == ptr) {
        return -1;
    }
    ptr += strlen("ETC:FWD ");

    ptr = strtok(ptr, ";");
    for (unsigned char i = 1; i < requiredValueNum; ++i) {
        ptr = strtok(NULL, ";");
    }

    return strtol(ptr, NULL, 10);
}




