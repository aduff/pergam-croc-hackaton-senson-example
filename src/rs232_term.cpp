/*
 * rs232_term.c
 *
 *  Created on: Mar 4, 2018
 *      Author: Andrey Loshchilov
 */

#include "main.h"
#include "rs232_term.h"


ssize_t sPortListPorts( const char * namePrefix,
                        char portNameStructure[][SERIAL_PORT_MAX_NAME_LEN],
                        ssize_t portNameStructurePosition)
{
    char portName[SERIAL_PORT_MAX_NAME_LEN];
    ssize_t portNameStructureIterator = portNameStructurePosition;

    for (ssize_t i = 0; i < SERIAL_PORTS_MAX_QTY; ++i) {
        //Clearing portname string
        memset(portName, 0, SERIAL_PORT_MAX_NAME_LEN);

        //Checking if the namePrefix length is ok
        if (strlen(namePrefix) >= SERIAL_PORT_MAX_NAME_LEN - 5) {
            printf("Serial port prefix length exceed limit: %d",
                    SERIAL_PORT_MAX_NAME_LEN - 5);
            return 0;
        }

        // Building portname string. It is safe to add 3 more
        // symbols to prefix because of check above
        sprintf(portName, "%s%d", namePrefix, i);

        // Trying to open port with name built above
        int fd = open(portName, O_RDWR | O_NOCTTY | O_NONBLOCK);
        if (fd > 0) {
            if (portNameStructureIterator <= SERIAL_PORTS_MAX_QTY) {
                close(fd);
                memset(portNameStructure[portNameStructureIterator],
                                         0, SERIAL_PORT_MAX_NAME_LEN);
                strcat(portNameStructure[portNameStructureIterator],
                        portName);
                portNameStructureIterator++;
            }
        }
        //else          // disabled due to not loose ports following after
        //    break;    // disconnected ones

    }

    return portNameStructureIterator;
}


int sPortOpen(char *portName)
{
    int fd = 0;

    fd = open(portName, O_RDWR, O_NOCTTY | O_NDELAY);
    if (fd < 0) {
        perror("Couldn't open COM Port..");
    }
    else {
        fcntl(fd, F_SETFL, 0);
    }

    return fd;
}



int sPortClose(int fd, struct termios *tioStruct_backup)
{
    int error = 0;

    error |= tcsetattr(fd, TCSANOW, tioStruct_backup);
    close(fd);

    return error;
}



int sPortConfigure(int fd, struct termios *tioStruct, struct termios *tioStruct_backup, speed_t speed, unsigned char timeout)
{
    int error = 0;

// Clear structures
    memset(tioStruct, 0, sizeof(struct termios));
    memset(tioStruct_backup, 0, sizeof(struct termios));

// Get current parameters and saving it
    error |= tcgetattr(fd, tioStruct_backup);
    *tioStruct = *tioStruct_backup;

// Creating structure with required Port parameters
    error |= cfsetispeed(tioStruct, speed);
    error |= cfsetospeed(tioStruct, speed);

// CONTROL flags
    tioStruct->c_cflag |= (CLOCAL | CREAD);

    tioStruct->c_cflag &= ~PARENB;  // No Parity check
    tioStruct->c_cflag &= ~CSTOPB;  // 1 Stop bit
    tioStruct->c_cflag &= ~CSIZE;   // Masking symbol size
    tioStruct->c_cflag |= CS8;      // Symbol size: 8 bit

    //tioStruct->c_cflag &= ~CRTSCTS;

// LINE flags
    tioStruct->c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG | IGNCR);  // Uncanonical (raw) mode

// INPUT flags
    tioStruct->c_iflag &= ~INPCK;   // No Parity check

// OUTPUT flags
   // tioStruct->c_oflag &= ~OPOST;   // Raw (unformatted) output

// Control symbols
   if (timeout > 0) {
       tioStruct->c_cc[VMIN] = 0;
       tioStruct->c_cc[VTIME] = timeout;
   }

    error |= tcsetattr(fd, TCSANOW, tioStruct);

    return error;
}


long sPortWrite(int fd, const void *buffer, unsigned long size, unsigned long uDelay)
{
    long bytesWritten = 0;

    for (unsigned long i = 0; i < size; ++i) {
        bytesWritten += write(fd, (char *)buffer + i, 1);
        usleep(uDelay);
    }

    return bytesWritten;
}

void sPortWriteMult (
        int             fds[SERIAL_PORTS_MAX_QTY],
        const void      *buffer,
        unsigned long   size,
        unsigned long   uDelay)
{
    for (ssize_t i = 0; i < SERIAL_PORTS_MAX_QTY; ++i) {
        if (fds[i] > 0) {
            sPortWrite(fds[i], buffer, size, uDelay);
        }
    }

    return;
}









