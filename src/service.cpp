/*
 * service.c
 *
 *  Created on: Mar 4, 2018
 *      Author: Andrey Loshchilov
 */
#include "main.h"
#include "service.h"

void misusage(char * path)
{
    printf("\nToo few arguments. Use --help.\nUsage: %s device.\n\nAvailable devices:\n", path);
    system("ls --color /dev/ttyS* /dev/ttyUSB* /dev/ttyAMA*");
    printf("\n");
}

void helpINeedSomebodiesHelp(void)
{
    printf("\nSome fucking software name here.\n\n/"
            "Available keys:\n\t/"
            "-p, --port\tSpecify here port with sensor here.");
}


void getDateTime_str(char * strDateTime, ssize_t maxLength, char * format)
{
    time_t  timeRaw;
    struct tm *timeStruct;
        
    memset(strDateTime, 0, strlen(strDateTime));
    time(&timeRaw);
    timeStruct = localtime(&timeRaw);
    

    strftime(strDateTime, maxLength, format, timeStruct);

}

void getDateTimeWithMs_str(char * strDateTime, ssize_t maxLength, char * format, int wantMs)
{
    time_t          timeRaw;
    struct tm       *timeStruct;
    struct timeb    millisec;
    char millisecString[16] = { 0, };

    memset(strDateTime, 0, strlen(strDateTime));
    time(&timeRaw);
    ftime(&millisec);
    timeStruct = localtime(&timeRaw);

    strftime(strDateTime, maxLength, format, timeStruct);

    if (wantMs) {
        sprintf(millisecString, ".%03u", millisec.millitm);
        strcat(strDateTime, millisecString);
    }
}

FILE * createFile(char * filePath, char * fileName, char * mode)
{
    char  buffer[128] = { 0, };
    FILE  *fp = NULL;

    for (unsigned char i = 0; i < 2; ++i) {
        sprintf(buffer, "%s%s", filePath, fileName);
        fp = fopen(buffer, mode);
        if (fp == NULL) {
            if (i == 0) {
                sprintf(buffer, "mkdir -p %s", filePath);       // trying to create path tree
                system(buffer);
                sleep(1);
            }
        }
        else
            break;
    }

    return fp;
}

ssize_t addToFile(char * filePath, char * fileName, char * bufferToWrite)
{
    char    buffer[128] = { 0, };
    FILE    *fp = NULL;
    ssize_t bytesWritten, closeCode;

    sprintf(buffer, "%s%s", filePath, fileName);
    fp = fopen(buffer, "a");
    if (fp == NULL)
        return -1;

    bytesWritten = fprintf(fp, bufferToWrite);
    
    closeCode = fclose(fp);
    if (closeCode != 0)
        bytesWritten = closeCode;

    return bytesWritten;
}



unsigned long buildGnrioMessage(char * buffer, unsigned long concentration,
        float latitude, float longitude)
{
    unsigned long messageLength = 0;

    return messageLength;
}

ssize_t minOfTwo(ssize_t arg1, ssize_t arg2)
{
    if (arg1 < arg2)
        return arg1;
    else
        return arg2;
}

ssize_t maxOfTwo(ssize_t arg1, ssize_t arg2)
{
    if (arg1 < arg2)
        return arg1;
    else
        return arg2;
}



