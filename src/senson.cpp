/*
 * senson.cpp
 *
 *  Created on: May 25, 2019
 *      Author: null
 */

//#include "main.h"
#include "senson.h"
#include "rs232_term.h"
#include "devices.h"
#include "service.h"
#include "config.h"

bool sensVerifyPort(const char * portName, struct termios * tio_b, struct Senson * senStruct)
{
    bool isOk = false;
    bool blinkBackup = senStruct->blinkLed;
    struct termios tio = { 0, };

    memcpy(senStruct->portName, portName,
            minOfTwo(strlen(portName), SENSON_STRUCT_FIELD_LENGTH));

    senStruct->fd = sPortOpen(senStruct->portName);
    if (0 == senStruct->fd)
        return isOk;
    else
        isOk = true;
    sPortConfigure(senStruct->fd, &tio, tio_b, SENSON_UART_BR, 4);

    // add indication as query is too long;
    senStruct->blinkLed = true;

    if (0 != sensCmd(senStruct, SENSON_CMD_MAN_ID)) {
        if (NULL != strstr(senStruct->responseBuffer, SENSON_MANUF_NAME)) {
            memcpy(senStruct->manufacturer, senStruct->responseBuffer,
                   minOfTwo(strlen(senStruct->responseBuffer), SENSON_STRUCT_FIELD_LENGTH));
        }
    }
    else {
        senStruct->blinkLed = blinkBackup;
        printf("Senson was not found at %s\n", senStruct->portName);
        return false;
    }
    senStruct->blinkLed = blinkBackup;

    return isOk;
}

bool sensInit(const char * portName, struct termios * tio_b, struct Senson * senStruct)
{
    bool isOk = false;
    bool blinkBackup = senStruct->blinkLed;
    struct termios tio = { 0, };

    if (0 == senStruct->fd) {
    memcpy(senStruct->portName, portName,
            minOfTwo(strlen(portName), SENSON_STRUCT_FIELD_LENGTH));

    senStruct->fd = sPortOpen(senStruct->portName);
    if (0 == senStruct->fd)
        return isOk;
    else
        isOk = true;
    sPortConfigure(senStruct->fd, &tio, tio_b, SENSON_UART_BR, 4);
    }

    // add indication as query is too long;
    senStruct->blinkLed = true;

    if (NULL == strstr(senStruct->manufacturer, SENSON_MANUF_NAME)) {
        if (0 != sensCmd(senStruct, SENSON_CMD_MAN_ID))
            memcpy(senStruct->manufacturer, senStruct->responseBuffer,
                    minOfTwo(strlen(senStruct->responseBuffer), SENSON_STRUCT_FIELD_LENGTH));
        else {
            senStruct->blinkLed = blinkBackup;
            return false;
        }
    }

    if(0 != sensCmd(senStruct, SENSON_CMD_SN))
        memcpy(senStruct->serialNum, senStruct->responseBuffer,
                minOfTwo(strlen(senStruct->responseBuffer), SENSON_STRUCT_FIELD_LENGTH));
    else {
            senStruct->blinkLed = blinkBackup;
            return false;
        }

    if(0 != sensCmd(senStruct, SENSON_CMD_MAN_DATE))
        memcpy(senStruct->dateManuf, senStruct->responseBuffer,
                minOfTwo(strlen(senStruct->responseBuffer), SENSON_STRUCT_FIELD_LENGTH));
    else {
            senStruct->blinkLed = blinkBackup;
            return false;
        }

    if(0 != sensCmd(senStruct, SENSON_CMD_CALIB_DATE))
        memcpy(senStruct->dateCalib, senStruct->responseBuffer,
                minOfTwo(strlen(senStruct->responseBuffer), SENSON_STRUCT_FIELD_LENGTH));
    else {
            senStruct->blinkLed = blinkBackup;
            return false;
        }

    if(0 != sensCmd(senStruct, SENSON_CMD_CAS)) {
        memcpy(senStruct->componentCas, senStruct->responseBuffer,
                minOfTwo(strlen(senStruct->responseBuffer), SENSON_STRUCT_FIELD_LENGTH));
    }
    else {
            senStruct->blinkLed = blinkBackup;
            return false;
        }

    if(0 != sensCmd(senStruct, SENSON_CMD_SENS_TYPE))
        memcpy(senStruct->sensorType, senStruct->responseBuffer,
                minOfTwo(strlen(senStruct->responseBuffer), SENSON_STRUCT_FIELD_LENGTH));
    else {
            senStruct->blinkLed = blinkBackup;
            return false;
        }

    if(0 != sensCmd(senStruct, SENSON_CMD_UNIT))
        memcpy(senStruct->unit, senStruct->responseBuffer,
                minOfTwo(strlen(senStruct->responseBuffer), SENSON_STRUCT_FIELD_LENGTH));
    else {
            senStruct->blinkLed = blinkBackup;
            return false;
        }

    if(0 != sensCmd(senStruct, SENSON_CMD_THRES_UPPER))
        senStruct->limitUpper = atof(senStruct->responseBuffer);
    else {
            senStruct->blinkLed = blinkBackup;
            return false;
        }

    if(0 != sensCmd(senStruct, SENSON_CMD_THRES_LOWER))
            senStruct->limitLower = atof(senStruct->responseBuffer);
    else {
            senStruct->blinkLed = blinkBackup;
            return false;
        }

    senStruct->blinkLed = blinkBackup;

    return isOk;
}

void sensInitMult(struct Senson senStruct[SENS_MAX_QTY])
{
    bool checkResult = true;

    for (ssize_t i = 0; i < SENS_MAX_QTY; ++i) {
        if (senStruct[i].fd > 0) {
            checkResult &= (NULL != strstr(senStruct[i].manufacturer, SENSON_MANUF_NAME));
            senStruct[i].blinkLed = true;
        }
    }

    if (checkResult) {
        sensCmdMult(senStruct, SENSON_CMD_MAN_ID, 1000);
        for (ssize_t i = 0; i < SENS_MAX_QTY; ++i) {
            if (senStruct[i].fd > 0) {
                memcpy(senStruct[i].manufacturer, senStruct[i].responseBuffer,
                       minOfTwo(strlen(senStruct[i].responseBuffer), SENSON_STRUCT_FIELD_LENGTH));
            }
            else {
                continue;
            }
        }
    }

    sensCmdMult(senStruct, SENSON_CMD_SN, 1000);
    for (ssize_t i = 0; i < SENS_MAX_QTY; ++i) {
        if (senStruct[i].fd > 0) {
            memcpy(senStruct[i].serialNum, senStruct[i].responseBuffer,
                   minOfTwo(strlen(senStruct[i].responseBuffer), SENSON_STRUCT_FIELD_LENGTH));
        }
        else {
            continue;
        }
    }

    sensCmdMult(senStruct, SENSON_CMD_MAN_DATE, 1000);
    for (ssize_t i = 0; i < SENS_MAX_QTY; ++i) {
        if (senStruct[i].fd > 0) {
            memcpy(senStruct[i].dateManuf, senStruct[i].responseBuffer,
                   minOfTwo(strlen(senStruct[i].responseBuffer), SENSON_STRUCT_FIELD_LENGTH));
        }
        else {
            continue;
        }
    }

    sensCmdMult(senStruct, SENSON_CMD_CALIB_DATE, 1000);
    for (ssize_t i = 0; i < SENS_MAX_QTY; ++i) {
        if (senStruct[i].fd > 0) {
            memcpy(senStruct[i].dateCalib, senStruct[i].responseBuffer,
                   minOfTwo(strlen(senStruct[i].responseBuffer), SENSON_STRUCT_FIELD_LENGTH));
        }
        else {
            continue;
        }
    }

    sensCmdMult(senStruct, SENSON_CMD_CAS, 1000);
    for (ssize_t i = 0; i < SENS_MAX_QTY; ++i) {
        if (senStruct[i].fd > 0) {
            memcpy(senStruct[i].componentCas, senStruct[i].responseBuffer,
                   minOfTwo(strlen(senStruct[i].responseBuffer), SENSON_STRUCT_FIELD_LENGTH));
        }
        else {
            continue;
        }
    }

    sensCmdMult(senStruct, SENSON_CMD_SENS_TYPE, 1000);
    for (ssize_t i = 0; i < SENS_MAX_QTY; ++i) {
        if (senStruct[i].fd > 0) {
            memcpy(senStruct[i].sensorType, senStruct[i].responseBuffer,
                   minOfTwo(strlen(senStruct[i].responseBuffer), SENSON_STRUCT_FIELD_LENGTH));
        }
        else {
            continue;
        }
    }

    sensCmdMult(senStruct, SENSON_CMD_UNIT, 1000);
    for (ssize_t i = 0; i < SENS_MAX_QTY; ++i) {
        if (senStruct[i].fd > 0) {
            memcpy(senStruct[i].unit, senStruct[i].responseBuffer,
                   minOfTwo(strlen(senStruct[i].responseBuffer), SENSON_STRUCT_FIELD_LENGTH));
        }
        else {
            continue;
        }
    }

    sensCmdMult(senStruct, SENSON_CMD_THRES_UPPER, 1000);
    for (ssize_t i = 0; i < SENS_MAX_QTY; ++i) {
        if (senStruct[i].fd > 0) {
            senStruct[i].limitUpper = atof(senStruct[i].responseBuffer);
        }
        else {
            continue;
        }
    }

    sensCmdMult(senStruct, SENSON_CMD_THRES_LOWER, 1000);
    for (ssize_t i = 0; i < SENS_MAX_QTY; ++i) {
        if (senStruct[i].fd > 0) {
            senStruct[i].limitLower = atof(senStruct[i].responseBuffer);
        }
        else {
            continue;
        }
    }

    for (ssize_t i = 0; i < SENS_MAX_QTY; ++i) {
        if (senStruct[i].fd > 0)
            senStruct[i].blinkLed = false;
    }


    return;
}

void sensDeInit(struct Senson * senStruct, struct termios * tio_b)
{
    if (senStruct->fd > 0) {
        sPortClose(senStruct->fd, tio_b);
        memset(senStruct, 0, sizeof(struct Senson));
    }

    return;
}

void sensDeinitMult(struct Senson senStruct[SENS_MAX_QTY], struct termios tio_b[SENS_MAX_QTY])
{
    for (ssize_t i = 0; i < SENS_MAX_QTY; ++i) {
        if (senStruct[i].fd > 0) {
            sensDeInit(&senStruct[i], &tio_b[i]);
        }
    }

    return;
}

const ssize_t sensCmd(struct Senson * senStruct, const char * command)
{
    ssize_t bytesToRead = 0;
    char respPrefix[8] = { 0, };
    char buffer[128] = { 0, };
    char * respPtr = buffer;

    if (senStruct->blinkLed)
            ledToggle(LED_SENSOR);

    memset(senStruct->responseBuffer, 0, SENSON_STRUCT_BUF_SIZE);

    memcpy(respPrefix, command, 5);
    respPrefix[2] = 'A';
    strcat(respPrefix, " ");

    tcflush(senStruct->fd, TCIFLUSH);

    sPortWrite(senStruct->fd, command, strlen(command), 5);
    //bytesToRead = waitForMessageAtPort(senStruct->fd, 6, 100);
    usleep(500 * 1000);

    if (senStruct->blinkLed)
        ledToggle(LED_SENSOR);

    usleep(500 * 1000);

    if (senStruct->blinkLed)
            ledToggle(LED_SENSOR);
    ioctl(senStruct->fd, FIONREAD, &bytesToRead);
    read(senStruct->fd, buffer, minOfTwo(bytesToRead, 120));

    // CHeck if error returned
    if (NULL != strstr(buffer, SENSON_CMD_ERROR_RESPONCE)) {
        printf("Error returned: %s\n", respPtr);
        memset(senStruct->responseBuffer, 0, SENSON_STRUCT_BUF_SIZE);
        return 0;
    }

    // CHeck if response prefix is correct
    if (NULL == strstr(buffer, respPrefix)) {
        printf("Response prefix is incorrect: %s\n", respPtr);
        memset(senStruct->responseBuffer, 0, SENSON_STRUCT_BUF_SIZE);
        return 0;
    }

    respPtr += strlen(respPrefix);
    removeEol(respPtr);
    if (strlen(respPtr) < SENSON_STRUCT_BUF_SIZE)
        strcpy(senStruct->responseBuffer, respPtr);
    else
        printf("Response is too long: %s", respPtr);

    if (senStruct->blinkLed)
            ledToggle(LED_SENSOR);

    return strlen(senStruct->responseBuffer);
}

void sensCmdMult(struct Senson senStruct[SENS_MAX_QTY], const char* command, ssize_t mDelay)
{
    bool blinkLedLocal = false;
    ssize_t bytesToRead = 0;
    char respPrefix[8] = { 0, };
    char buffer[SENS_MAX_QTY][128] = { 0, };
    char * respPtr[SENS_MAX_QTY];

    // Setting up pointers and flushing buffers for opened ports
    for (ssize_t i = 0; i < SENS_MAX_QTY; ++i) {
        respPtr[i] = buffer[i];
        if (senStruct[i].fd > 0)
            tcflush(senStruct[i].fd, TCIFLUSH);
        blinkLedLocal |= senStruct[i].blinkLed;
    }

    // switching diod if needsed
    //if (blinkLedLocal)
    //    ledToggle(LED_SENSOR);

    // Building response prefix
    memcpy(respPrefix, command, 5);
    respPrefix[2] = 'A';
    strcat(respPrefix, " ");

    // Writing commands to all opened ports
    for (ssize_t i = 0; i < SENS_MAX_QTY; ++i) {
        if (senStruct[i].fd > 0) {
            sPortWrite(senStruct[i].fd, command, strlen(command), 5);
        }
    }

    // Waiting reply for 1 second and blinking

    for (ssize_t blink_iter = 0; blink_iter < mDelay/10; ++blink_iter) {
        usleep(10 * 1000);
        if (blinkLedLocal && (blink_iter % 25 == 0))
            ledToggle(LED_SENSOR);
    }
    // Reading from all opened ports
    for (ssize_t i = 0; i < SENS_MAX_QTY; ++i) {
        if (senStruct[i].fd > 0) {
            ioctl(senStruct[i].fd, FIONREAD, &bytesToRead);
            read(senStruct[i].fd, buffer[i], minOfTwo(bytesToRead, 120));
        }
    }

    // Processing all the responces and saving to corresponding structure
    for (ssize_t i = 0; i < SENS_MAX_QTY; ++i) {
        if (senStruct[i].fd > 0) {
            // CHeck if error returned
            if (NULL != strstr(buffer[i], SENSON_CMD_ERROR_RESPONCE)) {
                printf("Error returned: %s @ port %s\n",
                        respPtr[i], senStruct[i].portName);
                memset(senStruct[i].responseBuffer, 0, SENSON_STRUCT_BUF_SIZE);
                continue;
            }

            // CHeck if response prefix is correct
            if (NULL == strstr(buffer[i], respPrefix)) {
                printf("Response prefix is incorrect: %s @ port %s\n",
                        respPtr[i], senStruct[i].portName);
                memset(senStruct[i].responseBuffer, 0, SENSON_STRUCT_BUF_SIZE);
                continue;
            }

            respPtr[i] += strlen(respPrefix);
            removeEol(respPtr[i]);

            if (strlen(respPtr[i]) < SENSON_STRUCT_BUF_SIZE)
                strcpy(senStruct[i].responseBuffer, respPtr[i]);
            else
                printf("Response is too long: %s @ port %s\n",
                        respPtr[i], senStruct[i].portName);
        }
    }

    //if (blinkLedLocal)
    //    ledToggle(LED_SENSOR);

    return;
}

const char * sensPrintInfo(struct Senson * senStruct)
{
    memset(senStruct->responseBuffer, 0, SENSON_STRUCT_BUF_SIZE);

    sprintf(senStruct->responseBuffer,
                "Port: %s\n",       senStruct->portName);

    sprintf(senStruct->responseBuffer + strlen(senStruct->responseBuffer),
            "Manufacturer: %s\n",       senStruct->manufacturer);

    if (SENSON_STRUCT_BUF_SIZE - strlen(senStruct->responseBuffer) > 50)
        sprintf(senStruct->responseBuffer + strlen(senStruct->responseBuffer),
                "Serial number: %s\n",      senStruct->serialNum);

    if (SENSON_STRUCT_BUF_SIZE - strlen(senStruct->responseBuffer) > 50)
        sprintf(senStruct->responseBuffer + strlen(senStruct->responseBuffer),
                "Manufacturing date: %s\n", senStruct->dateManuf);

    if (SENSON_STRUCT_BUF_SIZE - strlen(senStruct->responseBuffer) > 50)
        sprintf(senStruct->responseBuffer + strlen(senStruct->responseBuffer),
                "Calibration date: %s\n",   senStruct->dateCalib);

    if (SENSON_STRUCT_BUF_SIZE - strlen(senStruct->responseBuffer) > 50)
        sprintf(senStruct->responseBuffer + strlen(senStruct->responseBuffer),
                "Component: %s (CAS: %s)\n",
                senStruct->componentFrindlyName, senStruct->componentCas);

    if (SENSON_STRUCT_BUF_SIZE - strlen(senStruct->responseBuffer) > 20)
        sprintf(senStruct->responseBuffer + strlen(senStruct->responseBuffer),
                "Unit: %s\n",               senStruct->unit);

    if (SENSON_STRUCT_BUF_SIZE - strlen(senStruct->responseBuffer) > 30)
        sprintf(senStruct->responseBuffer + strlen(senStruct->responseBuffer),
                    "Upper limit: %.03f\n",      senStruct->limitUpper);

    if (SENSON_STRUCT_BUF_SIZE - strlen(senStruct->responseBuffer) > 30)
        sprintf(senStruct->responseBuffer + strlen(senStruct->responseBuffer),
                        "Lower limit: %.03f\n",      senStruct->limitLower);

    if (SENSON_STRUCT_BUF_SIZE - strlen(senStruct->responseBuffer) > 30)
        sprintf(senStruct->responseBuffer + strlen(senStruct->responseBuffer),
                        "Panic threshold: %.03f\n",      senStruct->panicThreshold);

    return senStruct->responseBuffer;
}

bool sensGetConc(struct Senson * senStruct)
{
    memset(senStruct->responseBuffer, 0, SENSON_STRUCT_BUF_SIZE);

    if(0 != sensCmd(senStruct, SENSON_CMD_CONC_QUERY))
            senStruct->conc = atof(senStruct->responseBuffer);
    else {
            return false;
    }

    return true;
}

const char * sensGetConcMult(struct Senson senStruct[SENS_MAX_QTY], bool * panic)
{
    bool panicLocal = false;
    static char buffer[256];

    memset(buffer, 0, 256);
    // Quering all available sensors in parallel
    sensCmdMult(senStruct, SENSON_CMD_CONC_QUERY, 300);

    // Saving data from responceBuffer to conc field
    for (ssize_t i = 0; i < SENS_MAX_QTY; ++i) {
        if (senStruct[i].fd > 0) {
            senStruct[i].conc = -9999.0f;
            if (strlen(senStruct[i].responseBuffer) > 0) {
                senStruct[i].conc = atof(senStruct[i].responseBuffer);
                panicLocal |= (senStruct[i].conc >= senStruct[i].panicThreshold);
            }
            // Building sensor string
            if ((senStruct[i].conc >= -9999.0f) && (strlen(buffer) + 60 < 256)) {
                if (senStruct[i].conc < 0)
                    senStruct[i].conc = 0;
                sprintf(buffer + strlen(buffer), "%s:%.05f[%s]",
                        senStruct[i].componentFrindlyName, senStruct[i].conc, senStruct[i].unit);
            }
            if (i < (SENS_MAX_QTY - 1)) {
                if (senStruct[i+1].fd > 0) {
                    if (strlen(buffer) + 2 < 1024)
                        sprintf(buffer + strlen(buffer), "_");
                }
            }
        }
        else {
            continue;
        }
    }
    *panic = panicLocal;

    return (const char *)buffer;
}

void sensSetupLimits(struct Senson senStruct[SENS_MAX_QTY], const char * pathToConfig)
{
    char    paramName[32] = { 0, };
    char    buffer[CONFIG_LINE_BUF_SIZE] = { 0, };
    char    defaultBuffer[16] = { 0, };
    double  thresholdLocal = -1;

    for (ssize_t sens_i = 0; sens_i < SENS_MAX_QTY; ++sens_i) {
        if (senStruct[sens_i].fd > 0) {

            // Clearing buffers
            memset(paramName, 0, CONFIG_LINE_BUF_SIZE);
            memset(buffer, 0, 16);
            //memset(defaultBuffer, 0, 16);

            // Preparing buffers
            thresholdLocal = senStruct[sens_i].limitUpper;
            sprintf(defaultBuffer, "%f", thresholdLocal);
            strcat(paramName, (const char *)("conc"));
            memcpy(paramName + strlen(paramName),
                    senStruct[sens_i].componentCas,
                    minOfTwo(32 - strlen(paramName), strlen(senStruct[sens_i].componentCas)));

            configReadParameterStr(pathToConfig, paramName, buffer, defaultBuffer);

            senStruct[sens_i].panicThreshold = atof(buffer);
            printf("%f\n", senStruct[sens_i].panicThreshold);
        }
    }

    return;
}

void sensSetupFriendlyNames(struct Senson senStruct[SENS_MAX_QTY], const char * pathToConfig)
{
    char    paramName[32] = { 0, };
    char    buffer[CONFIG_LINE_BUF_SIZE] = { 0, };

    for (ssize_t sens_i = 0; sens_i < SENS_MAX_QTY; ++sens_i) {
        if (senStruct[sens_i].fd > 0) {

            // Clearing buffers
            memset(senStruct[sens_i].componentFrindlyName, 0, SENSON_STRUCT_FIELD_LENGTH);
            memset(paramName, 0, CONFIG_LINE_BUF_SIZE);
            memset(buffer, 0, 16);

            // Preparing buffers
            strcat(paramName, (const char *)("fname"));
            memcpy(paramName + strlen(paramName),
                    senStruct[sens_i].componentCas,
                    minOfTwo(32 - strlen(paramName), strlen(senStruct[sens_i].componentCas)));

            configReadParameterStr(pathToConfig, paramName, buffer, senStruct[sens_i].componentCas);

            memcpy(senStruct[sens_i].componentFrindlyName, buffer,
                    SENSON_STRUCT_FIELD_LENGTH - 2);
        }
    }

    return;
}
