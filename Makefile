#==========================================================================
#
# Copyright (C) 2016 Peter Shevchenko <developer@cxemotexnika.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#==========================================================================
.PHONY:	test project all clean

#--------------------------------------------------------------------------
#	start user settings 
#--------------------------------------------------------------------------
#VERSION					:= Debug
VERSION				:= Release

PRJ_NAME				:= pergam-logger

SRC						:= main.cpp
SRC						+= config.cpp devices.cpp rs232_term.cpp service.cpp senson.cpp

VPATH					:= 
INC						:= main.h
INC 					+= config.h devices.h rs232_term.h service.h senson.h
DEFINE					:= 
LIB						:= stdc++ m wiringPi pthread rt crypt

OPTIMIZE_LEVEL			:= 2
DEBUG_LEVEL				:= gdb

TOOL_PATH				:= 'D:/SysGCC/raspberry/arm-linux-gnueabihf'
SHELL					:= 'C:/MinGW/msys/1.0/bin/sh.exe'
#--------------------------------------------------------------------------
#	end user settings
#--------------------------------------------------------------------------

ROOT					:= $(shell pwd)

CROSS_PREFIX			:= arm-linux-gnueabihf-
CC						:= $(CROSS_PREFIX)gcc
AS						:= $(CROSS_PREFIX)gcc
LD						:= $(CROSS_PREFIX)gcc
CPPC					:= $(CROSS_PREFIX)g++
AR						:= $(CROSS_PREFIX)ar
RANLIB					:= $(CROSS_PREFIX)ranlib
SIZE					:= $(CROSS_PREFIX)size
OBJCOPY					:= $(CROSS_PREFIX)objcopy
OBJDUMP					:= $(CROSS_PREFIX)objdump

BIN_DIR					:= bin
DEBUG_DIR				:= Debug
RELEASE_DIR				:= Release
INC_DIR					:= inc
OBJ_DIR					:= obj
SRC_DIR					:= src

VPATH					+= $(INC_DIR) $(OBJ_DIR) $(SRC_DIR)
INCLUDE_PATH			:= $(INC) $(INC_DIR) $(COMM_INC_DIR) $(TOOL_PATH)/include
LIB_PATH				:= $(TOOL_PATH)/sysroot/usr/lib
LIB_RPATH				:= $(TOOL_PATH)/sysroot/lib/arm-linux-gnueabihf

LIBFLAGS				:= $(addprefix -L,$(LIB_PATH)) $(addprefix -l,$(LIB))	"-Wl,-rpath,$(LIB_RPATH)"
ARFLAGS					:= rcs
 
CCFLAGS					:= -Wall
CCFLAGS					+= $(addprefix -D,$(DEFINE)) $(addprefix -I,$(INCLUDE_PATH))
CCFLAGS					+= -ffunction-sections -fdata-sections
 
ASFLAGS					:= -Wall
ASFLAGS					+= $(addprefix -D,$(DEFINE)) $(addprefix -I,$(INCLUDE_PATH))

ifeq ($(VERSION),Debug)
IMAGE					:= $(BIN_DIR)/$(DEBUG_DIR)/$(PRJ_NAME)
CCFLAGS					+= -g$(DEBUG_LEVEL) -O0 -DDEBUG
ASFLAGS					+= -g$(DEBUG_LEVEL) -O0 -DDEBUG
endif

ifeq ($(VERSION),Release)
IMAGE					:= $(BIN_DIR)/$(RELEASE_DIR)/$(PRJ_NAME)
CCFLAGS					+= -O$(OPTIMIZE_LEVEL)
ASFLAGS					+= -O0
endif

CPPCFLAGS				:= $(CCFLAGS)
CPPCFLAGS				+= -x c++ -Weffc++
CPPCFLAGS				+= -std=gnu++0x

LDFLAGS					:= -Wl,-Map,$(IMAGE).map,--cref -Wl,--gc-sections

OBJECTS					:= $(addprefix $(OBJ_DIR)/$(VERSION)/,$(patsubst %.c, %.o,$(filter %.c,$(SRC))))
OBJECTS					+= $(addprefix $(OBJ_DIR)/$(VERSION)/,$(patsubst %.cpp, %.o,$(filter %.cpp,$(SRC))))
OBJECTS					+= $(addprefix $(OBJ_DIR)/$(VERSION)/,$(patsubst %.s, %.o,$(filter %.s,$(SRC))))
OBJECTS					+= $(addprefix $(OBJ_DIR)/$(VERSION)/,$(patsubst %.S, %.o,$(filter %.S,$(SRC))))

#--------------------------------------------------------------------------
#	targets
#--------------------------------------------------------------------------

test: 
	$(CC) --version
	$(MAKE) --version
	@echo $(OBJECTS)
	@echo $(IMAGE)
		
project:
	test  -d $(BIN_DIR) || mkdir $(BIN_DIR) $(BIN_DIR)/$(DEBUG_DIR) $(BIN_DIR)/$(RELEASE_DIR)
	test  -d $(OBJ_DIR) || mkdir $(OBJ_DIR) $(OBJ_DIR)/$(DEBUG_DIR) $(OBJ_DIR)/$(RELEASE_DIR)
	test  -d $(INC_DIR) || mkdir $(INC_DIR)
	test  -d $(SRC_DIR) || mkdir $(SRC_DIR)

all: $(VERSION)
	
$(VERSION): exe lst size
	@echo "--------------------- COMPLETE -----------------------"

exe:$(IMAGE)

lst:$(IMAGE).lst

size:$(IMAGE)
	@echo
	@echo $@
	@echo "------------------------------------------------------"
	$(SIZE) $(IMAGE)

$(IMAGE).lst:$(IMAGE)
	@echo
	@echo $@
	@echo "------------------------------------------------------"
	$(OBJDUMP) -h -S -z $<  > $@

$(IMAGE):$(OBJECTS)
	@echo
	@echo $@
	@echo "------------------------------------------------------"
	$(LD) $(CCFLAGS) $(LDFLAGS) $^ -o $@ $(LIBFLAGS)

$(OBJ_DIR)/$(VERSION)/%.o:%.c
	@echo
	@echo $<
	@echo "------------------------------------------------------"
	$(CC) $(CCFLAGS) -MD -c $< -o $@

$(OBJ_DIR)/$(VERSION)/%.o:%.cpp
	@echo
	@echo $<
	@echo "------------------------------------------------------"
	$(CPPC) $(CPPCFLAGS) -MD -c $< -o $@

$(OBJ_DIR)/$(VERSION)/%.o:%.s
	@echo
	@echo $<
	@echo "------------------------------------------------------"
	$(AS) $(ASFLAGS) -c $< -o $@
	
$(OBJ_DIR)/$(VERSION)/%.o:%.S
	@echo
	@echo $<
	@echo "------------------------------------------------------"
	$(AS) $(ASFLAGS) -c $< -o $@

include $(wildcard $(OBJ_DIR)/$(VERSION)/*.d)

clean:
	rm -f $(OBJECTS)
	rm -f $(patsubst %.o, %.d,$(OBJECTS))
	rm -f $(IMAGE) $(IMAGE).map $(IMAGE).lst
	@echo "--------------------- COMPLETE -----------------------"
	
#==========================================================================
#	End Of File
#==========================================================================
