/*
 * rs232_term.h
 *
 *  Created on: Mar 4, 2018
 *      Author: Andrey Loshchilov
 */

#ifndef INC_RS232_TERM_H_
#define INC_RS232_TERM_H_

#define SERIAL_PORTS_MAX_QTY        16
#define SERIAL_PORT_MAX_NAME_LEN    32



/**********************************
 ******* External functions *******
 **********************************/

extern ssize_t sPortListPorts(  const char * namePrefix,
                                char portNameStructure[][SERIAL_PORT_MAX_NAME_LEN],
                                ssize_t portNameStructurePosition  );

extern int  sPortOpen        (char *portName);
extern int  sPortClose       (int fd, struct termios *tioStruct_backup);

extern int  sPortConfigure (
        int             fd,
        struct termios  *tioStruct,
        struct termios  *tioStruct_backup,
        speed_t         speed,
        unsigned char   timeout);

extern long sPortWrite (
        int             fd,
        const void      *buffer,
        unsigned long   size,
        unsigned long   uDelay);

extern void sPortWriteMult (
        int             fds[SERIAL_PORTS_MAX_QTY],
        const void      *buffer,
        unsigned long   size,
        unsigned long   uDelay);
















#endif /* INC_RS232_TERM_H_ */
