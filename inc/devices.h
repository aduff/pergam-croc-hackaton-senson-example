#ifndef _DEVICES_H_
#define _DEVICES_H_

#define READ_ERROR_NOT_COMPLETED    -100
#define READ_ERROR_OVERREADING      -200
#define READ_ERROR_NO_GPS           -201

#define PARSE_ERROR_INCORRECT_MSG   -50


#define READ_BUFFER_SIZE            512


/*
* if msTimeout is non-zero, waits specified time for any bytes at RX buffer;
* minimal non-zero timeout is 100 ms
*
* if msTimeout is zero, just returns number of bytes at RX buffer at the time.
*/
extern ssize_t bytesAtSerialPort(int fd, ssize_t msTimeout);


extern ssize_t waitForMessageAtPort(int fd, ssize_t minimalLength, ssize_t msTimeout);

/*
* Scan long from string
*/
extern ssize_t parseOutput(char * stringToParse, unsigned int requiredValueNum);

/*
* Set speed from config
*/
extern speed_t selectSpeed(long speedLong, speed_t defaultSpeed);

/*
* Remove EOL symbols
*/
extern ssize_t removeEol(char * target);

/*
 * Receives message from Sensor. expects NMEA-style message with correct CheckSum
 * If Sensor hanges, it will return NaN-ed message.
 */
extern ssize_t getMessageSensor(int fd, char * buffer, ssize_t bufferSize);

/*
 * Receives message from GPS receiver. Setup to get ONLY GxRMC message.
 * If no full and valid message, returns previous good one.
 */
extern ssize_t getActualNmeaMessage(int fd, char * buffer, ssize_t bufferSize,
        struct nmea_position * position);

/*
 * Toggles specified LED
 */
extern void ledToggle(int pin);

extern ssize_t getConcentrationLmmSensor(int fd, long sensorEnum);

extern long parseLmmStringForConcentration(char * string, long sensorEnum);


#endif // !_DEVICES_H_

