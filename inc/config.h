#ifndef _CONFIG_INI_
#define _CONFIG_INI_

#define MAX_CONFIG_VARIABLE_LEN 20
#define CONFIG_LINE_BUF_SIZE 256
#define MAX_LLIST_NAME_LEN 256
#define MAX_OUT_NAME_LEN 256



/*
* Read char * parameter
* Returns 0 if success
*/
extern ssize_t configReadParameterStr(const char * pathToConfig,      const char * parameterName,
                                        char * parameterValue,  const char * defaultValue);

/*
* Read long parameter
* Returns 0 if success
*/
extern ssize_t configReadParameterLong(char * pathToConfig,     char * parameterName,
                                        long * parameterValue,  long defaulValue);

/*
* removes spaces
*/
extern ssize_t removeSpaces(char * target);

/*
* Check if string contains parameter name and its value
*/
extern char * checkParameterString(char * target);

#endif // !_CONFIG_INI_

