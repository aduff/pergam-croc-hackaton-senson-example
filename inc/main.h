#ifndef _INC_MAIN_H_
#define _INC_MAIN_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <sys/timeb.h>
#include <inttypes.h>
#include <time.h>
//#include <string>
//#include <vector>
//#include <iterator>


#define OUT_STR_LENGTH          2048

#define LED_SENSOR              0
#define LED_GPS                 2
#define LED_EXT_TX              3
#define BTN_POWER               7




enum devices {
    DEVICE_SENSOR = 0,
    DEVICE_GPS,
    DEVICE_RMT,

    // place new devices here

    DEVICES_QTY,        // number of used devices
    DEVICES_DUMMY       // for debug or something else
};

enum sensorType {
    SENSOR_DEFAULT = 0,
    SENSOR_LMM,
    SENSOR_FALCON,
    SENSOR_SENSON_X4,

    // place new sensor types here

    SENSOR_QTY          // number of supported sensors
};



#endif // !_INC_MAIN_H_
