/*
 * senson.h
 *
 *  Created on: May 25, 2019
 *      Author: null
 */

#include "main.h"

#ifndef INC_SENSON_H_
#define INC_SENSON_H_

#define SENS_MAX_QTY                6


#define SENSON_CMD_SN                   "@RRIS\r\n"
#define SENSON_CMD_MAN_ID               "@RRIM\r\n"
#define SENSON_CMD_MAN_DATE             "@RRDS\r\n"
#define SENSON_CMD_CALIB_DATE           "@RRCD\r\n"
#define SENSON_CMD_CAS                  "@RRCA\r\n"
#define SENSON_CMD_SENS_TYPE            "@RRKS\r\n"
#define SENSON_CMD_UNIT                 "@RRUT\r\n"
#define SENSON_CMD_THRES_UPPER          "@RRCH\r\n"
#define SENSON_CMD_THRES_LOWER          "@RRCL\r\n"

#define SENSON_CMD_CONC_QUERY           "@RRDT\r\n"

#define SENSON_CMD_ERROR_RESPONCE       "@ER"

#define SENSON_STRUCT_FIELD_LENGTH      32
#define SENSON_STRUCT_BUF_SIZE          256

#define SENSON_MANUF_NAME               "Senson"

#define SENSON_COMP_CAS_H2S             "7782-06-4"
#define SENSON_COMP_NAME_H2S            "H2S"

#define SENSON_COMP_CAS_NO2             "10102-44-0"
#define SENSON_COMP_NAME_NO2            "NO2"

#define SENSON_COMP_CAS_CH4             "74-82-8"
#define SENSON_COMP_NAME_CH4            "CH4"

#define SENSON_COMP_CAS_SO2             "7746-09-5"
#define SENSON_COMP_NAME_SO2            "SO2"




const speed_t SENSON_UART_BR            = B9600;





struct Senson {
    int fd;
    char portName[SENSON_STRUCT_FIELD_LENGTH];

    char serialNum[SENSON_STRUCT_FIELD_LENGTH];
    char manufacturer[SENSON_STRUCT_FIELD_LENGTH];
    char dateManuf[SENSON_STRUCT_FIELD_LENGTH];         //format: "dd:mm:yy"
    char dateCalib[SENSON_STRUCT_FIELD_LENGTH];
    char componentCas[SENSON_STRUCT_FIELD_LENGTH];
    char componentFrindlyName[SENSON_STRUCT_FIELD_LENGTH];
    char sensorType[SENSON_STRUCT_FIELD_LENGTH];
    char unit[SENSON_STRUCT_FIELD_LENGTH];
    double limitUpper;
    double limitLower;
    double conc;
    double panicThreshold;

    bool blinkLed;

    char responseBuffer[SENSON_STRUCT_BUF_SIZE];

};

extern bool sensVerifyPort(const char * portName, struct termios * tio_b, struct Senson * senStruct);

extern bool sensInit(const char * portName, struct termios * tio_b, struct Senson * senStruct);

extern void sensInitMult(struct Senson senStruct[SENS_MAX_QTY]);

extern void sensDeInit(struct Senson * senStruct, struct termios * tio_b);

extern void sensDeinitMult(struct Senson senStruct[SENS_MAX_QTY], struct termios tio_b[SENS_MAX_QTY]);

extern const ssize_t sensCmd(struct Senson * senStruct, const char * command);

extern void sensCmdMult(struct Senson senStruct[SENS_MAX_QTY], const char* command, ssize_t mDelay);

extern bool sensGetConc(struct Senson * senStruct);

extern const char * sensGetConcMult(struct Senson senStruct[SENS_MAX_QTY], bool * panic);

extern const char * sensPrintInfo(struct Senson * senStruct);

extern void sensSetupLimits(struct Senson senStruct[SENS_MAX_QTY], const char * pathToConfig);

extern void sensSetupFriendlyNames(struct Senson senStruct[SENS_MAX_QTY], const char * pathToConfig);

#endif /* INC_SENSON_H_ */
