/*
 * service.h
 *
 *  Created on: Mar 4, 2018
 *      Author: Andrey Loshchilov
 *
 *      Module for miscellaneous functions..
 */

#ifndef INC_SERVICE_H_
#define INC_SERVICE_H_

/**********************************
 ******* External functions *******
 **********************************/

/*
 * Outputs simple usage description
 * args: path - path to program. Usually it is argv[0]
 */
extern void misusage(char * path);

/*
 * Outputs extended help
 */
extern void helpINeedSomebodiesHelp(void);

/*
* Used to obtain system time
*/
extern void getDateTime_str(char * strDateTime, ssize_t maxLength, char * format);

/*
* Used to obtain system time with ms
*/
extern void getDateTimeWithMs_str(char * strDateTime, ssize_t maxLength, char * format, int wantMs);

/*
* Used for create log file and close it.
*/
extern FILE * createFile(char * filePath, char * fileName, char * mode);

/*
* Used to add something to file
*/
extern ssize_t addToFile(char * filePath, char * fileName, char * bufferToWrite);


/*
* Build GNRIO message
*/
unsigned long buildGnrioMessage(char * buffer, unsigned long concentration,
        float latitude, float longitude);


extern ssize_t minOfTwo(ssize_t arg1, ssize_t arg2);

extern ssize_t maxOfTwo(ssize_t arg1, ssize_t arg2);

#endif /* INC_SERVICE_H_ */
